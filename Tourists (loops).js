const tourist = [];

tourist[0] = {
  firstName: "Mark",
  age: 19,
  trip: [
    { city: "Tbilisi", amount: 120 },
    { city: "London", amount: 200 },
    { city: "Rome", amount: 150 },
    { city: "Berlin", amount: 140 },
  ],
  tripExpense: {
    sum: 0,
    average: 0,
  },
};

tourist[1] = {
  firstName: "Bob",
  age: 21,
  trip: [
    { city: "Miami", amount: 90 },
    { city: "Moscow", amount: 240 },
    { city: "Vienna", amount: 100 },
    { city: "Riga", amount: 76 },
    { city: "Kiev", amount: 123 },
  ],
  tripExpense: {
    sum: 0,
    average: 0,
  },
};
tourist[2] = {
  firstName: "Sam",
  age: 22,
  trip: [
    { city: "Tbilisi", amount: 118 },
    { city: "Budapest", amount: 95 },
    { city: "Warsaw", amount: 210 },
    { city: "Vilnius", amount: 236 },
  ],
  tripExpense: {
    sum: 0,
    average: 0,
  },
};
tourist[3] = {
  firstName: "Anna",
  age: 20,
  trip: [
    { city: "New York", amount: 100 },
    { city: "Athen", amount: 240 },
    { city: "Sydney", amount: 50 },
    { city: "Tokyo", amount: 190 },
  ],
  tripExpense: {
    sum: 0,
    average: 0,
  },
};
tourist[4] = {
  firstName: "Alex",
  age: 23,
  trip: [
    { city: "Paris", amount: 96 },
    { city: "Tbilisi", amount: 134 },
    { city: "Madrid", amount: 76 },
    { city: "Marseille", amount: 210 },
    { city: "Minsk", amount: 158 },
  ],
  tripExpense: {
    sum: 0,
    average: 0,
  },
};

// 2) tourists over 21;
for (let i = 0; i < tourist.length; i++) {
  tourist[i].over21 = tourist[i].age >= 21;
}
// 3) has been in Georgia ?
for (let i = 0; i < tourist.length; i++) {
  for (let j = 0; j < tourist[i].trip.length; j++) {
    if (tourist[i].trip[j].city === "Tbilisi") {
      tourist[i].wasInGeorgia = true;
      break;
    } else {
      tourist[i].wasInGeorgia = false;
    }
  }
}
// 4),5) sum and average of amount;
for (let i = 0; i < tourist.length; i++) {
  for (let j = 0; j < tourist[i].trip.length; j++) {
    tourist[i].tripExpense.sum += tourist[i].trip[j].amount;
  }
  tourist[i].tripExpense.average =
    tourist[i].tripExpense.sum / tourist[i].trip.length;
}

// 6) the biggest expense
let tmpTourist = tourist[0];
for (let i = 1; i < tourist.length; i++) {
  tmpTourist =
    tmpTourist.tripExpense.sum > tourist[i].tripExpense.sum
      ? tmpTourist
      : tourist[i];
}

console.log(
  `The biggest expense --> ${tmpTourist.tripExpense.sum} has ${tmpTourist.firstName}`
);
