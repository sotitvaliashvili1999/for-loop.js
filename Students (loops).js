//*********************** Subject credits array ********************
const subject = [
  { name: "javascript", credit: 4 },
  { name: "react", credit: 7 },
  { name: "python", credit: 6 },
  { name: "java", credit: 3 },
];
subject.sumCredits = 0;
for (let i = 0; i < subject.length; i++) {
  subject.sumCredits += subject[i].credit;
}
// *********************** Students ***********************
const student = [];
student[0] = {
  firstName: "Jean",
  lastName: "Reno",
  age: 26,
  subject: [
    { name: "javascript", score: 62 },
    { name: "react", score: 57 },
    { name: "python", score: 88 },
    { name: "java", score: 90 },
  ],
  statistic: {
    sum: 0,
    average: 0,
    GPA: 0,
  },
};
student[1] = {
  firstName: "Klod",
  lastName: "Mone",
  age: 19,
  subject: [
    { name: "javascript", score: 77 },
    { name: "react", score: 52 },
    { name: "python", score: 92 },
    { name: "java", score: 67 },
  ],
  statistic: {
    sum: 0,
    average: 0,
    GPA: 0,
  },
};
student[2] = {
  firstName: "Van",
  lastName: "Gogh",
  age: 21,
  subject: [
    { name: "javascript", score: 51 },
    { name: "react", score: 98 },
    { name: "python", score: 65 },
    { name: "java", score: 70 },
  ],
  statistic: {
    sum: 0,
    average: 0,
    GPA: 0,
  },
};
student[3] = {
  firstName: "Dam",
  lastName: "Square",
  age: 36,
  subject: [
    { name: "javascript", score: 82 },
    { name: "react", score: 53 },
    { name: "python", score: 80 },
    { name: "java", score: 65 },
  ],
  statistic: {
    sum: 0,
    average: 0,
    GPA: 0,
  },
};
//*********************** Sum and average of scores***********************
for (let i = 0; i < student.length; i++) {
  for (let j = 0; j < student[i].subject.length; j++) {
    student[i].statistic.sum += student[i].subject[j].score;
  }
  student[i].statistic.average =
    student[i].statistic.sum / student[i].subject.length;
}
// *********************** G P A ***********************
const gp = [];
let gpSum = 0;
for (let i = 0; i < student.length; i++) {
  for (let j = 0; j < student[i].subject.length; j++) {
    if (student[i].subject[j].score >= 91) {
      gp[j] = 4;
    } else if (student[i].subject[j].score >= 81) {
      gp[j] = 3;
    } else if (student[i].subject[j].score >= 71) {
      gp[j] = 2;
    } else if (student[i].subject[j].score >= 61) {
      gp[j] = 1;
    } else if (student[i].subject[j].score >= 51) {
      gp[j] = 0.5;
    }
    gpSum += subject[j].credit * gp[j];
  }
  student[i].statistic.GPA = gpSum / subject.sumCredits;
  gpSum = 0;
}
// *********************** Highest GPA ***********************
let bestStudentWithGpa = student[0];
for (let i = 1; i < student.length; i++) {
  bestStudentWithGpa =
    bestStudentWithGpa.statistic.GPA > student[i].statistic.GPA
      ? bestStudentWithGpa
      : student[i];
}
console.log(
  `The highest GPA --> ${bestStudentWithGpa.statistic.GPA} has ${bestStudentWithGpa.firstName} ${bestStudentWithGpa.lastName} `
);
// *********************** Degree ***************************************
let avgOfAllStudents = 0;
for (let i = 0; i < student.length; i++) {
  avgOfAllStudents +=
    student[i].statistic.sum / (student.length * student[0].subject.length);
}
for (let i = 0; i < student.length; i++) {
  student[i].degree =
    student[i].statistic.average > avgOfAllStudents
      ? "Red Diploma"
      : "Enemy of People";
}

// *********************** the best student over 21 with average mark***********************
let counter = 0;
let tmp;
let bestAvg;
for (let i = 0; i < student.length; i++) {
  tmp = student[i];
  if (student[i].age > 21) {
    for (let j = i + 1; j < student.length; j++) {
      if (student[j].age > 21) {
        tmp =
          tmp.statistic.average > student[j].statistic.average
            ? student[i]
            : student[j];
      }
    }
    bestAvg = tmp;
  } else {
    counter++;
  }
  if (bestAvg) {
    break;
  }
}
if (counter === student.length) {
  console.log("No one is over 21");
} else {
  console.log(
    `The best is ${bestAvg.firstName} ${bestAvg.lastName} over 21 with his average mark --> ${bestAvg.statistic.average};`
  );
}
// *********************** The  best in  front-end; **********************************
for (let i = 0; i < student.length; i++) {
  student[i].statistic.FrontMarkAvg = 0;
  for (let j = 0; j < 2; j++) {
    student[i].statistic.FrontMarkAvg += student[i].subject[j].score / 2;
  }
}
let bestInFront = student[0];
for (let i = 1; i < 4; i++) {
  bestInFront =
    bestInFront.statistic.FrontMarkAvg > student[i].statistic.FrontMarkAvg
      ? bestInFront
      : student[i];
}
console.log(
  `The best is ${bestInFront.firstName} ${bestInFront.lastName}  in Front-end subjects;`
);
